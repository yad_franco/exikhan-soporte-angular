import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'regresaString'
})
export class RegresaStringPipe implements PipeTransform {

  transform(value: string): string {
    var re = / /gi; 
        var newstr = value.replace(re, ""); 
        return newstr;
  }

}

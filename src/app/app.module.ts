import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
// RUTAS
import { APP_ROUTING } from "./app.routes";
import { DocumentacionComponent } from './components/documentacion/documentacion.component';
import { HomeCardsComponent } from './shared/home-cards/home-cards.component';
import { AtajoTagsImagenesComponent } from './shared/atajo-tags-imagenes/atajo-tags-imagenes.component';
import { RegresaStringPipe } from './pipes/regresa-string.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    DocumentacionComponent,
    HomeCardsComponent,
    AtajoTagsImagenesComponent,
    RegresaStringPipe
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-atajo-tags-imagenes',
  templateUrl: './atajo-tags-imagenes.component.html',
  styleUrls: ['./atajo-tags-imagenes.component.scss']
})
export class AtajoTagsImagenesComponent implements OnInit {
  // para logos de empresas en el home
  imagenesLogosEmpresas = [];
  //
  rutaParaLogosEmpresas = "../../../assets/images/logos/";
  claseSCSSLogos ="logosEmpresas";
  //
  constructor() {
    this.imagenesLogosEmpresas = [{ nombre: "grupocco.jpg" }, { nombre: 'airfrance.jpg' }, { nombre: 'ba.jpg' },{ nombre: 'capital.jpg' },{ nombre: 'iberia.jpg' },
                                  { nombre: 'japan.jpg' }, { nombre: 'klm.jpg'}, { nombre: 'macargo.jpg' }, { nombre: 'schryver.jpg' },{ nombre: 'varig.jpg' } ]
   }
  ngOnInit(): void {
  }

}

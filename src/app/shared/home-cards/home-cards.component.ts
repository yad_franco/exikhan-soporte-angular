import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-cards',
  templateUrl: './home-cards.component.html',
  styleUrls: ['./home-cards.component.scss']
})
export class HomeCardsComponent implements OnInit {
  paraCard : any [] = [];
  constructor() {
    this.paraCard = [
      {
        who: 'FOR MANAGERS',
        icon: 'fas fa-user-tie'
      },
      {
        who: 'FOR USERS',
        icon: 'fas fa-user'
      }
    ]
   }

  ngOnInit(): void {
  }

}

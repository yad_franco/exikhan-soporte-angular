import { DocumentacionComponent } from './components/documentacion/documentacion.component';

import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
//
 const APP_ROUTERS: Routes = [
    // el path es el nombre que irá en la barra de navegación , el otro es el componente que tomará
    { path: 'home', component: HomeComponent},
    { path: 'documentacion', component: DocumentacionComponent},
    // estos son para que cualquier otra dirección escrita me direccione al home
    { path: '', pathMatch: 'full', redirectTo:'home'},
    { path: '**', pathMatch: 'full', redirectTo:'home'}
]
// esta etiqueta se usa en app.component.html
export const APP_ROUTING = RouterModule.forRoot(APP_ROUTERS);
